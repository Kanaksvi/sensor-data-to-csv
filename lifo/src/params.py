from attrdict import AttrDict
import boto3
from botocore.exceptions import ClientError
import logging
import yaml


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('%(asctime)s-%(levelname)s-%(filename)s : %(message)s'))
logger.addHandler(handler)






def load_config_yaml(config_path = "config.yaml"):
    with open(config_path) as f:
        configs = yaml.load(f, Loader=yaml.Loader)  # config is dict
        cfg = AttrDict(configs)
    logger.debug(f"Configurations Loaded :{cfg}")
    return cfg



def initialise_ssm_client(cfg):
    
    env = cfg.env
    # creds = AttrDict(cfg.credentials[env])
    

    AWS_ACCESS_KEY_ID = creds.AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY = creds.AWS_SECRET_ACCESS_KEY
    

    ssm = boto3.client(service_name='ssm', region_name = 'ap-south-1',
                            aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    return ssm, env






def load_parameters(ssm, env):

    path = f'/trip_score/{env}'
    try:
        ssm_response = ssm.get_parameters_by_path(Path=path, WithDecryption=True)
    except ClientError as e:
        #logger.exception(f'SSM ClientError: {str(e)}')
        ssm_response = {
            'Parameters': {},
        }

    param_json = ssm_response.get('Parameters', {})
    #logger.debug(f'Param Length: {len(param_json)} ...')
    next_token = ssm_response.get('NextToken', '')
    while next_token:
        ssm_response = ssm.get_parameters_by_path(Path=f'/daily_trip_stats/dev', WithDecryption=True, NextToken=next_token)
        param_json.extend(ssm_response['Parameters'])
        #logger.debug(f'Param Length: {len(param_json)} ...')
        next_token = ssm_response.get('NextToken', '')
    
    params = {}
    for _param in param_json:
        key = _param['Name'].split('/')[-1]
        value = _param['Value']
        params[key] = value
    logger.debug(f"Parameters : {params}")      
    return params


def get_params(config_path = "config.yaml"):
    cfg = load_config_yaml(config_path)
    ssm, env = initialise_ssm_client(cfg)
    params = load_parameters( ssm, env)
    return cfg, params



if __name__ == "__main__":
    cfg, params = get_params()
    breakpoint()








# AWS_ACCESS_KEY_ID = "AKIAR6RMMOMPH6GOHMEP"
# AWS_SECRET_ACCESS_KEY = "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"

# AWS_ACCESS_KEY_ID = "AKIAR6RMMOMPDBFYHNME"
# AWS_SECRET_ACCESS_KEY = "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"

# AWS_ACCESS_KEY_ID = "AKIAR6RMMOMPGA3OUVCF"
# AWS_SECRET_ACCESS_KEY = "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"