import logging
# import logging_loki

# def get_loki_handler():
#     handler = logging_loki.LokiHandler(
#         url="http://15.206.242.75:3100/loki/api/v1/push", 
#         tags={"kafka_pipeline": "stage"},
#         auth=("admin", "Admin@2020"),
#         version="1",
#     )
#     return handler


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    handler = get_loki_handler( )
    handler.setFormatter(logging.Formatter('%(asctime)-15s %(levelname)-8s %(module)s %(message)s'))
    logger.addHandler(handler)
    return logger