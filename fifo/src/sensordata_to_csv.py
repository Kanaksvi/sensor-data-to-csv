from configparser import ConfigParser
# from dotenv import load_dotenv
from json import loads as json_deserialize
from logging import getLogger
from os import environ, makedirs
import threading
from uuid import uuid1

# load_dotenv('../docker.env')  # take environment variables from .env.
makedirs('logs', exist_ok=True)

import boto3
from botocore.exceptions import ClientError
from confluent_kafka import Consumer

from custom_logger import logger_init
logger_init(environ['DBAI_ENV'] + '_SensorToCsvFIFO')

from flatten import save_to_csv
from file_manager import upload_files, clean_files

logger = getLogger(environ['DBAI_ENV'] + '_SensorToCsvFIFO')
kafka_logger = getLogger('KafkaLog')


def load_parameters(app):
    ssm = boto3.client('ssm')
    try:
        ssm_response = ssm.get_parameters_by_path(Path=f"/{app}/{environ['DBAI_ENV']}", WithDecryption=True)
    except ClientError as e:
        logger.exception(f'SSM ClientError: {str(e)}')
        ssm_response = {
            'Parameters': {},
        }
    param_lst = ssm_response.get('Parameters', [])
    logger.debug(f'Param Length: {len(param_lst)} ...')

    next_token = ssm_response.get('NextToken', '')

    while next_token:
        try:
            ssm_response = ssm.get_parameters_by_path(Path=f"/{app}/{environ['DBAI_ENV']}", WithDecryption=True, NextToken=next_token)
        except ClientError as e:
            logger.exception(f'SSM ClientError: {str(e)}')
            ssm_response = {
                'Parameters': {},
            }

        param_lst.extend(ssm_response['Parameters'])
        logger.debug(f'Param Length: {len(param_lst)} ...')

        next_token = ssm_response.get('NextToken', '')

    for _param in param_lst:
        environ[_param['Name'].replace(f"/{app}/{environ['DBAI_ENV']}/", '')] = _param['Value']

    # Defensive check to see if environment variable exists
    for param_name in ['FIFO_TOPIC', 'CSV_TOPIC', 'KAFKA_SERVER', 'KAFKA_USERNAME', 'KAFKA_PASSWORD', 'S3_SECRET_ACCESS_KEY', 'S3_DEFAULT_REGION', 'S3_ACCESS_KEY_ID']:
        if environ.get(param_name, 'aws-error') == 'aws-error':
            logger.exception(f'{param_name} missing!')
            return False
    return True


def load_config():
    """Read config.ini file from current directory

    Returns:
        ConfigParser: default params used in code
    """
    logger.info('Reading module cofig')
    config = ConfigParser()
    config.read('config.ini')
    return config


if __name__ == '__main__':
    logger.info('Waking up')
    all_set = load_parameters('sensor2csv')
    assert all_set
    makedirs('tmp', exist_ok=True)

    config = load_config()

    producer_conf = dict(config['kafka_producer_config'])
    producer_conf['bootstrap.servers'] = environ.get('KAFKA_SERVER', 'unknown')
    producer_conf['sasl.username'] = environ.get('KAFKA_USERNAME', 'unknown')
    producer_conf['sasl.password'] = environ.get('KAFKA_PASSWORD', 'unknown')

    consumer_conf = dict(config['kafka_consumer_config'])
    consumer_conf['bootstrap.servers'] = environ.get('KAFKA_SERVER', 'unknown')
    consumer_conf['sasl.username'] = environ.get('KAFKA_USERNAME', 'unknown')
    consumer_conf['sasl.password'] = environ.get('KAFKA_PASSWORD', 'unknown')
    # print(kafka_config)

    # import json
    # with open('sample.json', 'r')as _f:
    #     sample = json.load(_f)

    # print(len(sample))
    # file_lst = save_to_csv(sample)
    # print(file_lst)
    # threads = []
    # for i, fname in enumerate(file_lst):
    #     print(i)
    #     t = threading.Thread(target=upload_files, args=(producer_conf, fname,))
    #     t.start()
    #     threads.append(t)

    #     if len(threads) > 2:
    #         for t in threads:
    #             t.join()
    #         threads = []

    logger.info('Connecting to Kafka')
    video_consumer = Consumer(consumer_conf, logger=kafka_logger)
    video_consumer.subscribe([environ.get('FIFO_TOPIC', 'unknown')])

    threads = []
    while True:
        try:
            msg = video_consumer.consume(
                num_messages=int(config['internal']['batch_size']),
                timeout=int(config['internal']['timeout'])
            )
            raw_data = []
            for _m in msg:
                if not _m.error():
                    raw_data.append(json_deserialize(_m.value()))
            if not len(raw_data):
                continue
            logger.debug(f'UUID: {uuid1().hex}')
            logger.debug(f'Length of raw data: {len(raw_data)}')
            file_lst = save_to_csv(raw_data)
            logger.debug(f'List of files dumped: {file_lst}')

            for idx in range(0, len(file_lst), 3):

                batch = file_lst[idx:idx+3]
                logger.debug(f'Batch: {batch}')
                for _name in batch:
                    t = threading.Thread(target=upload_files, args=(producer_conf, _name,), daemon=True)
                    t.start()
                    threads.append(t)

                for t in threads:
                    t.join()
                threads = []

            # Clean tmp files
            clean_files()

        except Exception as e:
            logger.info('Some unknown error occured.')
            logger.exception(e)
