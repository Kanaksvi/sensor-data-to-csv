from confluent_kafka import Consumer,  Producer
import json
import time
import pandas as pd
from time import time
import logging
from configparser import ConfigParser
from os import environ, makedirs
from uuid import uuid1

import boto3
from botocore.exceptions import ClientError

from custom_logger import logger_init
logger_init(environ['DBAI_ENV'] + '_SensorToCsvLIFO')

from data_clean import flatten_sensor_data, clean_df
from upload_s3 import download_file, upload_csv

logger = logging.getLogger(environ['DBAI_ENV'] + '_SensorToCsvLIFO')
kafka_logger = logging.getLogger('KafkaLog')

sensor_group_id = "10001"
key_group_id = "10002"

makedirs('temp', exist_ok=True)
makedirs('logs', exist_ok=True)

def load_parameters(app):
    ssm = boto3.client('ssm')
    try:
        ssm_response = ssm.get_parameters_by_path(Path=f"/{app}/{environ['DBAI_ENV']}", WithDecryption=True)
    except ClientError as e:
        logger.exception(f'SSM ClientError: {str(e)}')
        ssm_response = {
            'Parameters': {},
        }
    param_lst = ssm_response.get('Parameters', [])
    logger.debug(f'Param Length: {len(param_lst)} ...')

    next_token = ssm_response.get('NextToken', '')

    while next_token:
        try:
            ssm_response = ssm.get_parameters_by_path(Path=f"/{app}/{environ['DBAI_ENV']}", WithDecryption=True, NextToken=next_token)
        except ClientError as e:
            logger.exception(f'SSM ClientError: {str(e)}')
            ssm_response = {
                'Parameters': {},
            }

        param_lst.extend(ssm_response['Parameters'])
        logger.debug(f'Param Length: {len(param_lst)} ...')

        next_token = ssm_response.get('NextToken', '')

    for _param in param_lst:
        environ[_param['Name'].replace(f"/{app}/{environ['DBAI_ENV']}/", '')] = _param['Value']

    # Defensive check to see if environment variable exists
    for param_name in ['LIFO_TOPIC', 'CSV_TOPIC', 'KAFKA_SERVER', 'KAFKA_USERNAME', 'KAFKA_PASSWORD', 'S3_SECRET_ACCESS_KEY', 'S3_DEFAULT_REGION', 'S3_ACCESS_KEY_ID']:
        if environ.get(param_name, 'aws-error') == 'aws-error':
            logger.exception(f'{param_name} missing!')
            return False
    return True


all_set = load_parameters('sensor2csv')
assert all_set

config = ConfigParser()
config.read('config.ini')

# from dotenv import load_dotenv
# load_dotenv('../docker.env')  # take environment variables from .env.

kafka_config = dict(config['kafka_consumer_config'])
kafka_config['bootstrap.servers'] = environ.get('KAFKA_SERVER', 'unknown')
kafka_config['sasl.username'] = environ.get('KAFKA_USERNAME', 'unknown')
kafka_config['sasl.password'] = environ.get('KAFKA_PASSWORD', 'unknown')

sensor_data_consumer = Consumer(kafka_config, logger=kafka_logger)
sensor_data_consumer.subscribe([environ.get('LIFO_TOPIC', 'unknown')])

kafka_config = dict(config['kafka_producer_config'])
kafka_config['bootstrap.servers'] = environ.get('KAFKA_SERVER', 'unknown')
kafka_config['sasl.username'] = environ.get('KAFKA_USERNAME', 'unknown')
kafka_config['sasl.password'] = environ.get('KAFKA_PASSWORD', 'unknown')
csv_key_producer = Producer(kafka_config, logger=kafka_logger)

while True:
    try:
        c_start = time()
        logger.info("Polling from consumer initiated")
        # msg = sensor_data_consumer.consume(num_messages = cfg.batch_size.sensor_data_con.num_messages, timeout = cfg.batch_size.sensor_data_con.timeout)
        msg = sensor_data_consumer.consume(
            num_messages=int(config['internal']['batch_size']),
            timeout=int(config['internal']['timeout'])
        )
        c_end = time()       
        logger.debug(f"Total no of msgs consumed : {len(msg)}")
        # logger.debug(f"Total time : {c_end - c_start}", extra={'tags':{f'{cfg.env}' : 'timer_sensor_data_consumer'}})

        
    except Exception as e:
        logger.error('Polling Failed')
        msg = []

    try:
        sensor_data = [ json.loads( m.value()) for m in msg if not m.error()]
    except Exception as e :
        sensor_data = []


    if sensor_data:
        logger.info(f'UUID: {uuid1().hex}')
        logger.info("Sensor Data Cleaning and Flatenning initiated")
        sensor_df = flatten_sensor_data(sensor_data)
        df = clean_df(sensor_df)
        logger.info("Data Cleaning and Flattening Finished")
        # print(sensor_data[-1])
        # print(sensor_data[0])

        # df = pd.DataFrame( flatten_sensor_data)
        # df.timestamp = pd.to_datetime(df.timestamp, unit='ms')
        # print(df['timestamp'])

        df_grp = df.groupby('device_id')
        for name, df_device in df_grp:
            # print(name)
            # print(df_device['timestamp'])
            dev_id = df_device['device_id'].iloc[0]
            # print(f"temp/temp_{dev_id}.csv")
            obj = download_file( f"temp/temp_{dev_id}.csv")
            if obj:
                # print('Got an object')
                temp_df = pd.read_csv( obj)
            else :
                # print('New DF')
                temp_df = pd.DataFrame()
            if not temp_df.empty:
                df_device = temp_df.append(df_device, ignore_index = True)
            # print(df_device['timestamp'])
                
            
            df_device.timestamp = pd.to_datetime(df_device.timestamp, format='%Y-%m-%dT%H:%M:%S.%f') 
            df_device.reset_index(drop=True, inplace = True)
            df_device['minutes'] = df_device['timestamp'].dt.floor('Min')
            # df_device.to_csv('df_device.csv')
            minute_groups = df_device.groupby( 'minutes')

            for i,(time_min ,min_grp) in enumerate(minute_groups):

                if i == len( minute_groups) - 1:
                    # print('i == len(minute_groups) - 1')
                    min_grp.sort_values(by='timestamp', inplace=True)
                    min_grp.reset_index(drop=True, inplace = True)
                    min_grp.drop('minutes', axis = 1, inplace = True)
                    
                    min_grp.drop_duplicates( subset = ['timestamp'], inplace= True)
                    logger.debug(f"S3 key: temp/temp_{df_device['device_id'].iloc[0]}.csv")
                    # print(min_grp.shape)
                    upload_csv(min_grp, f"temp/temp_{df_device['device_id'].iloc[0]}.csv")
                    # print(min_grp['timestamp'])
                    # min_grp.to_csv(f"temp_{dev_id}.csv" , index = False)
                    
                else :
                    # print('i != len(minute_groups) - 1')
                    hist_obj = download_file(f"{dev_id}/{dev_id}_{str(time_min).split('+')[0]}.csv")
                
                    if hist_obj:
                        hist_df = pd.read_csv( hist_obj)
                    else :
                        hist_df = pd.DataFrame()
                    if not hist_df.empty:
                        min_grp = hist_df.append( min_grp, ignore_index = True)
                    
                    min_grp.timestamp = pd.to_datetime(df_device.timestamp, format='%Y-%m-%dT%H:%M:%S.%f')
                    min_grp.sort_values(by='timestamp', inplace=True)
                    min_grp.reset_index(drop=True, inplace = True)
                    min_grp.drop('minutes', axis = 1, inplace = True)

                    min_grp.drop_duplicates( subset = ['timestamp'], inplace= True)

                    
                    key_date = str(min_grp.timestamp.loc[0].date())
                    
                    key = f"{dev_id}/{key_date}/{dev_id}_{time_min.strftime('%Y-%m-%d_%H-%M')}.csv"
                    makedirs(f'{dev_id}/{key_date}', exist_ok=True)
                    # print('----------------------')
                    logger.debug(f'S3 Key: {key}')
                    upload_csv(min_grp, key)
                    # print(min_grp)
                    key_data = {'deviceId' : dev_id, 'csvKey' : key}
                    while True:
                        try:
                            csv_key_producer.poll(timeout=2)
                            csv_key_producer.produce(environ['CSV_TOPIC'], value=json.dumps(key_data), key = str(key_data['deviceId'])) #based on key( device id)
                            break 
                        except BufferError as buffer_error:
                            logger.error(f"{buffer_error} : Wait until queue has some free space")
                            time.sleep(1)
                        except Exception as e:
                            logger.error(f"Produce Error : {e}")
                    csv_key_producer.flush()

        # raise KeyboardInterrupt
