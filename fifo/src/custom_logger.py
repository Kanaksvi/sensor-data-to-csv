import logging
import logging.handlers
from os import environ
from threading import get_native_id


class ContainerNameFilter(logging.Filter):
    """ Logging filter for hostname in logs. """

    def __init__(self):
        logging.Filter.__init__(self)

    def filter(self, record):
        """ Add hostname to logging.Filter """
        record.containername = environ.get('HOSTNAME', 'unnamed_container')
        return True


class ThreadIdFilter(logging.Filter):
    """ Logging filter for hostname in logs. """

    def __init__(self):
        logging.Filter.__init__(self)

    def filter(self, record):
        """ Add hostname to logging.Filter """
        record.thread_id = get_native_id()
        return True


def logger_init(log_name):
    """ Create a logger. """
    handler = logging.handlers.RotatingFileHandler(
        'logs/' + log_name + '.log',
        maxBytes=50*1024*1024,
        backupCount=1,
    )

    handler.addFilter(ContainerNameFilter())
    handler.addFilter(ThreadIdFilter())
    formatter_str = '%(containername)s [%(levelname)s] %(asctime)s %(thread_id)s %(name)s %(funcName)s %(message)s'
    log_formatter = logging.Formatter(formatter_str)
    handler.setFormatter(log_formatter)

    dbai_logger = logging.getLogger(log_name)
    dbai_logger.addHandler(handler)
    dbai_logger.setLevel(logging.DEBUG)

    # return 
