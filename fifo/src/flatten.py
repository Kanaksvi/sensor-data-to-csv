from logging import getLogger
from os import environ

import pandas as pd

logger = getLogger(environ['DBAI_ENV'] + '_SensorToCsvFIFO')


def flatten_sensor_data(raw_data):
    """create a flatten dict from the nested raw data

    Args:
        raw_data (list): list of json containing sensor data from device

    Returns:
        list: list of dict, each dict containing flattened data
    """
    part_sensor_data = {
        'timestamp': [],
        'device_id': [],
        'vehicle_id': [],
        'client_id': [],
        'lat': [],
        'lng': [],
        'speed': [],
        'speed_meter': [],
        'veh_pitch': [],
        'veh_roll': [],
        'heading': [],
        'height': [],
        'xAccel': [],
        'yAccel': [],
        'zAccel': [],
        'xAngRate': [],
        'yAngRate': [],
        'zAngRate': [],
        'yaw': [],
        'pitch': [],
        'roll': [],
        'face_x1': [],
        'face_y1': [],
        'face_x2': [],
        'face_y2': [],
        'EAR': [],
        'MAR': [],
        'ETC': [],
        'drowsy': [],
        'drowsy_state': [],
        'distraction': [],
    }
    rms_data = {
        'timestamp': [],
        'device_id': [],
        # 'vehicle_id': [],
        # 'client_id': [],
        'object_class': [],
        'ttc': [],
        'alert': [],
        'object_x1': [],
        'object_y1': [],
        'object_x2': [],
        'object_y2': [],
        'rms_confidence': [],
    }

    logger.info('Flattening raw data')
    for _data in raw_data:
        try:
            ts = _data['timestamp']
            _id = _data['deviceId']
            part_sensor_data['timestamp'].append(ts)
            part_sensor_data['device_id'].append(_id)
        except KeyError:
            continue # Such data won't make any sense

        part_sensor_data['client_id'].append(_data.get('clientId', ''))
        part_sensor_data['vehicle_id'].append(_data.get('vehicleId', ''))

        try:
            part_sensor_data['speed'].append(_data['gnss']['speedData']['speed'])
        except KeyError:
            part_sensor_data['speed'].append('')

        try:
            part_sensor_data['speed_meter'].append(_data['gnss']['speedData']['speedMeter'])
        except KeyError:
            part_sensor_data['speed_meter'].append('')

        try:
            lat, lng = _data['gnss']['location']['coordinates']
            part_sensor_data['lat'].append(lat)
            part_sensor_data['lng'].append(lng)
        except (KeyError, ValueError, TypeError):
            part_sensor_data['lat'].append('')
            part_sensor_data['lng'].append('')

        _veh = _data.get('orientation', {})
        part_sensor_data['veh_pitch'] = _veh.get('pitch', '')
        part_sensor_data['veh_roll'] = _veh.get('roll', '')
        part_sensor_data['heading'] = _veh.get('heading', '')
        part_sensor_data['height'] = _veh.get('height', '')
        part_sensor_data['xAccel'] = _veh.get('xAccel', '')
        part_sensor_data['yAccel'] = _veh.get('yAccel', '')
        part_sensor_data['zAccel'] = _veh.get('zAccel', '')
        part_sensor_data['xAngRate'] = _veh.get('xAngRate', '')
        part_sensor_data['yAngRate'] = _veh.get('yAngRate', '')
        part_sensor_data['zAngRate'] = _veh.get('zAngRate', '')

        try:
            yaw, pitch, roll = _data['faceData']['headpose']
            part_sensor_data['yaw'].append(yaw)
            part_sensor_data['pitch'].append(pitch)
            part_sensor_data['roll'].append(roll)
        except (KeyError, ValueError, TypeError):
            part_sensor_data['yaw'].append('')
            part_sensor_data['pitch'].append('')
            part_sensor_data['roll'].append('')

        try:
            [[face_x1, face_y1], [face_x2, face_y2]] = _data['faceData']['pixelLocation']
            part_sensor_data['face_x1'].append(face_x1)
            part_sensor_data['face_y1'].append(face_y1)
            part_sensor_data['face_x2'].append(face_x2)
            part_sensor_data['face_y2'].append(face_y2)
        except (KeyError, ValueError, TypeError):
            part_sensor_data['face_x1'].append('')
            part_sensor_data['face_y1'].append('')
            part_sensor_data['face_x2'].append('')
            part_sensor_data['face_y2'].append('')

        face_data = _data.get('faceData', {})
        part_sensor_data['EAR'].append(face_data.get('ear', ''))
        part_sensor_data['MAR'].append(face_data.get('mar', ''))
        part_sensor_data['ETC'].append(face_data.get('ETC', ''))

        try:
            part_sensor_data['drowsy'].append(_data['faceAlertStats']['drowsycnt'])
        except KeyError:
            part_sensor_data['drowsy'].append('')

        try:
            part_sensor_data['drowsy_state'].append(_data['faceAlertStats']['drowsyState'])
        except KeyError:
            part_sensor_data['drowsy_state'].append('')

        try:
            part_sensor_data['distraction'].append(_data['faceAlertStats']['distractioncnt'])
        except KeyError:
            part_sensor_data['distraction'].append('')

        if len(_data.get('object', [])):
            for obj_data in _data['object']:
                try:
                    device_id = _data['deviceId']
                    timestamp = _data['timestamp']
                    ttc = obj_data['ttc']
                    [[object_x1, object_y1], [object_x2, object_y2]] = obj_data['pixelLocation']
                except (KeyError, ValueError):
                    continue # Again, such data won't make any sense

                rms_data['timestamp'].append(timestamp)
                rms_data['device_id'].append(device_id)
                rms_data['ttc'].append(ttc)
                rms_data['object_x1'].append(object_x1)
                rms_data['object_y1'].append(object_y1)
                rms_data['object_x2'].append(object_x2)
                rms_data['object_y2'].append(object_y2)
                # rms_data['client_id'].append(_data.get('clientId', ''))
                # rms_data['vehicle_id'].append(_data.get('vehicleId', ''))
                rms_data['alert'].append(obj_data.get('alertLevel', ''))
                rms_data['object_class'].append(obj_data.get('objName', ''))
                rms_data['rms_confidence'].append(obj_data.get('confidenceLevel', ''))

    logger.info('Created sensor and rsm dictionaries')
    return part_sensor_data, rms_data


def data_2_df(part_sensor_data, rms_data):
    """flatten dict to dataframe

    Args:
        part_sensor_data (dict): sensor data
        rms_data (dict): rms data

    Returns:
        Pandas DataFrame: converted and combined
    """
    logger.info('Combining sensor and rms data')
    # No point in optimizing since we need to simplify raw_data
    part_sensor_df = pd.DataFrame(part_sensor_data)
    rms_df = pd.DataFrame(rms_data)

    sensor_df = part_sensor_df.merge(rms_df, on=['timestamp', 'device_id'], how='outer').reset_index(drop=True)
    sensor_df.sort_values(by='timestamp', inplace=True)
    sensor_df.reset_index(drop=True, inplace=True)
    # sensor_df.drop(['index', 'level_0'], inplace=True)

    return sensor_df


def clean_df(df):
    """minor cleaning of dataframe

    Args:
        df (Pandas DataFrame): sensor data dataframe

    Returns:
        Pandas DataFrame: cleaned data
    """
    logger.info('Basic cleaning operations')
    try:
        df['epoch'] = pd.to_datetime(df.timestamp, unit='ms')
        df.timestamp = df.epoch.dt.tz_localize('UTC')
    except Exception as e:
        logger.exception(e)
        try:
            df.timestamp = pd.to_datetime(df.timestamp, format='%Y-%m-%dT%H:%M:%S.%fZ')
            df['epoch'] = df.timestamp.astype('int64') // 1e6
        except Exception as e:
            logger.exception(e)
            logger.debug(f'Original DataFrame:\n{df.to_string()}')
            return pd.DataFrame(columns=['device_id', 'timestamp'])

    df.sort_values(by='timestamp', inplace=True)
    df.reset_index(drop=True, inplace = True)
    df.timestamp = df.timestamp.dt.round('100L')

    # Need to convert them to numeric in order for interpolation to work
    df['face_x1'] = pd.to_numeric(df['face_x1'])
    df['face_y1'] = pd.to_numeric(df['face_y1'])
    df['face_x2'] = pd.to_numeric(df['face_x2'])
    df['face_y2'] = pd.to_numeric(df['face_y2'])
    df['object_x1'] = pd.to_numeric(df['object_x1'])
    df['object_y1'] = pd.to_numeric(df['object_y1'])
    df['object_x2'] = pd.to_numeric(df['object_x2'])
    df['object_y2'] = pd.to_numeric(df['object_y2'])
    df['ttc'] = pd.to_numeric(df['ttc'])
    df['speed'] = pd.to_numeric(df['speed'])

    df[[
        'face_x1',
        'face_y1',
        'face_x2',
        'face_y2'
    ]] = df[[
        'face_x1',
        'face_y1',
        'face_x2',
        'face_y2'
    ]].interpolate(method='pad', limit=5)

    df[[
        'ttc',
        'object_x1',
        'object_y1',
        'object_x2',
        'object_y2',
        'alert',
        'object_class'
    ]] = df[[
        'ttc',
        'object_x1',
        'object_y1',
        'object_x2',
        'object_y2',
        'alert',
        'object_class'
    ]].interpolate(method='pad', limit=3)

    df.speed.interpolate(limit=10, inplace=True)

    return df


def dump_csvs(sensor_df):
    """DataFrame to Csv

    Args:
        sensor_df (Pandas DataFrame): dataframe to dump

    Returns:
        list: list of dumped files
    """
    # sensor_df['timestamp'] = sensor_df['timestamp'].astype('datetime64[ns]')
    if sensor_df.empty: return []

    logger.info('Dumping all CSVs to local')
    sensor_grp = sensor_df.groupby(['device_id', pd.Grouper(key='timestamp',freq='1Min')])

    file_lst = []
    for key_pair, _g in sensor_grp:
        file_name = key_pair[0] + '_' + key_pair[1].strftime('%Y-%m-%d_%H-%M') + '.csv'
        _g.to_csv(file_name, index=False)
        file_lst.append(file_name)

    return file_lst


def save_to_csv(raw_data):
    """raw data to csv

    Args:
        raw_data (list): list of json sensor data

    Returns:
        list: list of dumped files
    """
    logger.info('Start the sorting process.')
    part_sensor_data, rms_data = flatten_sensor_data(raw_data)
    sensor_df = data_2_df(part_sensor_data, rms_data)
    df = clean_df(sensor_df)
    return dump_csvs(df)
