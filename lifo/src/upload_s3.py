import boto3
import botocore
from io import StringIO
from logging import getLogger
from os import environ

# logger = get_logger( 'kafka_pipeline')
logger = getLogger(environ['DBAI_ENV'] + '_SensorToCsvLIFO')

# bucket = 'kalfa-sensor-data-csv'
# bucket = 'dbai-stage-sensordata-test'

# bucket = 'dev-kafka-sensor-data-csv'
# AWS_ACCESS_KEY_ID = "AKIAR6RMMOMPER3LOKEA"
# AWS_SECRET_ACCESS_KEY = "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"


s3_client = boto3.client(
    's3',
    region_name=environ.get('S3_DEFAULT_REGION', 'region'),
    aws_access_key_id=environ.get('S3_ACCESS_KEY_ID', 'id'),
    aws_secret_access_key=environ.get('S3_SECRET_ACCESS_KEY', 'key'),
)

s3_resource= boto3.resource(
    service_name='s3',
    region_name=environ.get('S3_DEFAULT_REGION', 'region'),
    aws_access_key_id=environ.get('S3_ACCESS_KEY_ID', 'id'),
    aws_secret_access_key=environ.get('S3_SECRET_ACCESS_KEY', 'key')
)

# def upload_to_s3( filename, key):
#     try:
#         s3_client.upload_file( filename, bucket, key)
#         logger.info(f"Csv upload succes - {key}")

#     except botocore.exceptions.ClientError as error:
#         logger.error( f"ClientError : {error.response['Error']['Code']}")
#         logger.error(f"Csv upload failed - {key}")
#     except Exception as e:
#         logger.error(f"Csv upload failed - {key}")



def download_file( key):
    try : 
        obj = s3_client.get_object( Bucket = environ['S3_BUCKET'], Key = key)
        return obj['Body']
    except botocore.exceptions.ClientError as error:
        # logger.error( f"ClientError : {error.response['Error']['Code']}")
        logger.error('Error while downloading: {}'.format(error.response['Error']['Message']))
        # logger.error(f"Temp File not available - {key}")
        return ""
    except Exception:
        return ""


def upload_csv (df, key):
    csv_buffer = StringIO()
    df.to_csv(csv_buffer, index = False)
    # print(key)
    # df.to_csv('\\'.join(key.split('/')), index=False)
    logger.debug(environ.get('S3_BUCKET', 'bucket'), key)
    try:
        s3_resource.Object(environ['S3_BUCKET'], key).put(Body=csv_buffer.getvalue())
        # s3_client.upload_file(key, environ.get('S3_BUCKET', 'bucket'), key)
    
    except botocore.exceptions.ClientError as error:
        logger.error( f"ClientError : {error.response['Error']['Code']}")
        logger.error('Error Message: {}'.format(error.response['Error']['Message']))
        logger.error(f"Csv upload failed - {key}")

    except Exception as e:
        logger.exception(e)
        logger.error(f"Csv upload failed - {key}")

# def list_object():
#     keys = []
#     resp = s3_client.list_objects_v2(Bucket=bucket)
#     for obj in resp['Contents']:
#         keys.append(obj['Key'])
#     return keys

# def delete_all():
#     keys = list_object()
#     for key in keys:
#         s3_client.delete_object(Bucket=bucket, Key=key)
#     return keys


# if __name__ == "__main__":
#     while True:
#         keys = delete_all()
#         if len(keys) == 0:
#             break
