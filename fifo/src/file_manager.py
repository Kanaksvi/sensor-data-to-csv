from glob import glob
from json import dumps as json_serialize
from logging import getLogger
from os import environ, remove as remove_file
from time import sleep

import boto3
import pandas as pd
from confluent_kafka import Producer

logger = getLogger(environ['DBAI_ENV'] + '_SensorToCsvFIFO')
kafka_logger = getLogger('KafkaLog')


def connect_s3():
    """ Connect to S3 via default AWS credentials

    Returns:
        boto3 Client
    """
    logger.info('Connecting to S3')
    return boto3.client(
        's3',
        region_name=environ.get('S3_DEFAULT_REGION', 'region'),
        aws_access_key_id=environ.get('S3_ACCESS_KEY_ID', 'id'),
        aws_secret_access_key=environ.get('S3_SECRET_ACCESS_KEY', 'key'),
    )



def download_file(s3_cli, _key, local_path):
    """Download (any) S3 file to local path

    Args:
        s3_cli (boto3 Client): Connector
        _key (str): Source
        local_path (str): Destination
    """
    logger.debug(f'S3 file to download: {_key}')

    try:
        s3_cli.download_file(environ.get('S3_BUCKET', 'bucket'), _key, local_path)
        logger.debug(f'Downloaded {_key} to {local_path}')
    except s3_cli.exceptions.ClientError:
        logger.debug(f'Couldnt download {_key}')


def check_file_exists(s3_cli, _file):
    """Check if file is present in S3 main folder or tmp folder

    Args:
        s3_cli (boto3 Client): Connector
        _file (str): file to check

    Returns:
        bool: Yes or No
        str: path on S3 if present, else empty string
    """
    try:
        logger.info('Checking main folder in S3')
        device_id, dt, _ = _file.split('_')
        _path = f'{device_id}/{dt}/{_file}'
        s3_cli.head_object(
            Bucket=environ.get('S3_BUCKET', 'bucket'),
            Key=_path
        )
        return True, _path
    except s3_cli.exceptions.ClientError:
        logger.info('Checking temporary folder in S3')

    try:
        _path = 'tmp/' + _file
        s3_cli.head_object(
            Bucket=environ.get('S3_BUCKET', 'bucket'),
            Key=_path
        )
        return True, _path
    except s3_cli.exceptions.ClientError:
        logger.info('No existing copy on S3')
        return False, ''


def read_complete_file(s3_cli, _file):
    """_summary_

    Args:
        s3_cli (boto3 Client): Connector
        _file (str): file path

    Returns:
        bool: is file ready to be uploaded to main folder?
    """
    ret, current_path = check_file_exists(s3_cli, _file)

    if ret:
        local_path = 'tmp/' + _file
        download_file(s3_cli, current_path, local_path)
        tmp_df = pd.read_csv(local_path)
        updated_df = pd.read_csv(_file)
        df = pd.concat([tmp_df, updated_df]).reset_index(drop=True)
        df.to_csv(_file, index=False)

    # TODO: read like tect file and count lines; saves memory
    logger.info(f'Check file {_file}')
    df = pd.read_csv(_file)
    df.to_csv(_file, index=False)

    # Assuming atleast 55 seconds of data is present
    if len(df) > 550:
        return True
    return False


def upload_files(kafka_config, _file):
    """Upload file to S3. Also checks if some part of file already present on S3

    Args:
        kafka_config (dict): default params to kafka producer
        _file (str): current file
    """

    if not _file:
        return

    s3_cli = connect_s3()
    ready_to_process = read_complete_file(s3_cli, _file)
    if ready_to_process:
        device_id, dt, _ = _file.split('_')
        upload_path = f'{device_id}/{dt}/{_file}'
    else:
        upload_path = f'tmp/{_file}'

    upload_2_s3(s3_cli, _file, upload_path)

    if ready_to_process:
        # print('Push', _file)
        push_csv_name(kafka_config, upload_path)


def upload_2_s3(s3_cli, _file, upload_path):
    logger.debug(f'Upload path: {upload_path}')
    try:
        s3_cli.upload_file(_file, environ.get('S3_BUCKET', 'bucket'), upload_path)
    except s3_cli.exceptions.ClientError:
        logger.debug(f"Couldnt upload {_file} to {environ.get('S3_BUCKET', 'bucket')}/{upload_path}")
    logger.info(f'Detelting file {_file} freom local')
    remove_file(_file)


def clean_files(folder='tmp/', pattern='*.csv'):
    """Delete all files with matching patterns inside the folder

    Args:
        folder (str, optional): folder name with /. Defaults to 'tmp/'.
        pattern (str, optional): file pattern. Defaults to '*.csv'.
    """
    tmp_lst = glob(folder + pattern)
    for _file in tmp_lst:
        remove_file(_file)


def push_csv_name(kafka_config, _file):
    """Send final S3 path to kafka

    Args:
        kafka_config (dict): default kafka producer params
        _file (str): file path
    """
    logger.info('Pushing file name to Kafka')
    csv_key_producer = Producer(kafka_config, logger=kafka_logger)

    device = _file.split('_')[0]
    key_data = {'deviceId' : device, 'csvKey' : _file}
    try:
        csv_key_producer.poll(timeout=2)
        csv_key_producer.produce(environ['CSV_TOPIC'], value=json_serialize(key_data), key=device)
    except BufferError as buffer_error:
        logger.error(f"{buffer_error} : Wait until queue has some free space")
        sleep(1)
    except Exception as e:
        logger.error(f"Produce Error : {e}")
    csv_key_producer.flush()
